package ar.edu.cresta.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.cresta.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
	Usuario findByEmail(String email);
}
