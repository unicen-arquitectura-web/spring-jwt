package ar.edu.cresta.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import ar.edu.cresta.model.Usuario;
import ar.edu.cresta.model.dto.UsuarioDTO;

public interface DefaultUserService extends UserDetailsService {
	Usuario save(UsuarioDTO usuario_dto);
}
