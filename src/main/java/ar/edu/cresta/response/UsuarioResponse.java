package ar.edu.cresta.response;

import ar.edu.cresta.model.Usuario;

public class UsuarioResponse {

	private Usuario usuario;

	public UsuarioResponse(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
